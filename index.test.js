const socketIO=require('./index') // Burada proje fonksiyonlarına eriştik.


test('SHA-256 Test Block: ', () => {
  expect(socketIO.sha256("1234","Merhaba")).toBe("f394ca0285e06c4c399aa71989c2a35cc45ffd1cf2fddeb1680faa6f3514e8c2"); });
// Burada SHA-256 algoritması ile Anahtar ve Mesajın,
// doğru şifrelenip şifrelenmediğini test ettik.


test('SPN-16 Test Block: ', () => {
  expect(socketIO.spn16("Merhaba","1234")).toBe("0100111101010000"); });
// Burada SPN-16 algoritması ile anahtar ve Mesajın,
// doğru şifrelenip şifrelenmediğini test ettik.


test('XOR Test Block: ', () => {
  expect(socketIO.XOR("1","0")).toBe(false); });
// Burada SPN-16 algoritmasında kullanılan XOR’ lama işleminin,
// doğru yapılıp yapılmadığını test ettik.


test('AsciiToBinary Test Block: ', () => {
  expect(socketIO.asciiToBinary("Merhaba")).toBe("1001101 1100101 1110010 1101000 1100001 1100010 1100001 "); });
// Burada SPN-16 algoritmasında kullanılan Mesajın karakterlerinin,
// Ascii’ deki binary karşılığının doğru bulunup bulunmadığını test ettik.


test('StringToBinary Test Block: ', () => {
  expect(socketIO.stringToBinary("Merhaba")).toBe("1001101110010111100101101000110000111000101100001"); });
// Burada SPN-16 algoritmasında kullanılan Mesajın Ascii’ de
// binary koda çevrilmesi işleminin doğru yapılıp yapılmadığını test ettik.
